---
title: "Root-Me - Introduction to GraphQL Exploitation"
layout: "talks"
date: 2023-01-24
url: "/talks/graphql"
summary: "Talk given during the Club Root-Me about an introduction to GraphQL exploitation"
author: "Nishacid"
tags: 
- GraphQL
- Root-Me
- Talk
- Article
---

![miniature](/assets/images/articles/graphql/miniature.png)

During the Ethical Hacking Club organized by Root-Me, I had the opportunity to give an introduction to GraphQL exploitation. The replay of the conference is available on Root-Me's Youtube channel: 

[Club EH RM 04 - Intro to GraphQL Exploitation](https://www.youtube.com/watch?v=ypEL40YHG18)

The slides can be found [here](/assets/docs/ClubRM_04_GraphQL.pdf)