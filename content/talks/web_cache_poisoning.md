---
title: "Web Cache Poisoning 101"
layout: "talks"
date: 2024-02-23
url: "/talks/web_cache_poisoning"
summary: "Talk about Web Cache Poisoning"
author: "Nishacid"
tags: 
- Cache Poisoning
- Web
- XSS
- Talk
- Cache
---

<a href="/assets/docs/Web_Cache_Poisoning.pdf">
<img src="/assets/images/talks/cp/miniature.png" style="margin-left:0%;border:solid;border-color:#885FFF;">
</a>
