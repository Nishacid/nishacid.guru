---
title: "Web Enumeration : 404 to RCE"
layout: "talks"
date: 2023-12-21
url: "/talks/404_to_rce"
summary: "Talk presented at a Securimag session"
author: "Nishacid"
tags: 
- Enumeration
- Web
- Fuzz
- Talk
- Securimag
- RCE
---

<a href="/assets/docs/404_to_RCE_-_Securimag.pdf">
<img src="/assets/images/talks/404/miniature.png" style="margin-left:0%;border:solid;border-color:#885FFF;">
</a>
