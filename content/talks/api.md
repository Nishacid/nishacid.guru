---
title: "Root-Me - Introduction to API Testing"
layout: "talks"
date: 2024-01-18
url: "/talks/api"
summary: "Talk given during the Club Root-Me about an introduction to API Testing"
author: "Nishacid"
tags: 
- API
- Root-Me
- Talk
- Article
---

![miniature](/assets/images/articles/api/miniature.png)

During the Ethical Hacking Club organized by Root-Me, I had the opportunity to give an introduction to API Testing. The replay of the conference is available on Root-Me's Youtube channel: 

[Club EH RM 12 - Intro to API Testing](https://www.youtube.com/watch?v=RiKikKIibpk)

The slides can be found [here](/assets/docs/ClubRM_12_API_Pentesting.pdf)