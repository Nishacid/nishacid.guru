---
title: "Root-Me - Introduction to JSON Web Token Exploitation"
layout: "talks"
date: 2023-02-24
url: "/talks/jwt"
summary: "Talk given during the Club Root-Me about an introduction to JSON Web Token exploitation"
author: "Nishacid"
tags: 
- JSON Web Token
- JWT
- Root-Me
- Talk
- Article
---

![miniature](/assets/images/articles/jwt/miniature.png)

During the Ethical Hacking Club organized by Root-Me, I had the opportunity to give an introduction to JSON Web Token exploitation. The replay of the conference is available on Root-Me's Youtube channel: 

[Club EH RM 05 - Intro to JSON Web Token Exploitation](https://www.youtube.com/watch?v=d7wmUz57Nlg)

The slides can be found [here](/assets/docs/ClubRM_05_JSON_Web_Token.pdf)