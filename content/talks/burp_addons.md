---
title: "Addons adventure : Burp Suite customization"
layout: "talks"
date: 2023-08-26
url: "/talks/burp_addons"
summary: "Talk presented at the Barbhack 2023 rump session"
author: "Nishacid"
tags: 
- BurpSuite
- Addons
- Extensions
- Talk
- Barbhack
---

<a href="/assets/docs/Addons_Adventure_Burp_Suite_Customization.pdf">
<img src="/assets/images/talks/burp/miniature.png" style="margin-left:0%;border:solid;border-color:#885FFF;">
</a>
