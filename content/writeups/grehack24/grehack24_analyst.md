---
title: "GreHack 2024 CTF - Analyst"
layout: "writeups"
date: 2024-11-16
url: "/writeups/grehack_analyst"
summary: "Writeup of the forensics intro challenge 'Analyst' at the GreHack 2024 CTF."
author: "Nishacid"
tags: 
- Writeups
- GreHack
- GreHack24
- Challenge
- Forensic
- Log Analysis
ShowToc: true
TocOpen: true
ShowReadingTime: true
ShowBreadCrumbs: true
---

<img src="/assets/images/writeups/gh24/GH24_logocomplet.svg">

## Introduction

This year again, I was happy to be part of the organization committee for the [GreHack conference](https://grehack.fr/) and I created some challenges for the CTF. 
Organization was tricky this year, given that we had grown and sold almost 3x as many tickets as in previous years.
Thanks to all the participants, organizers and sponsors, the event was once again complety insane :fire: :green_heart:

## Challenge

- Name : `Analyst`
- Category : `Forensic / Intro`
- Difficulty : `Very Easy`
- Solves : `29`
- Points : `50`
- Author : `Nishacid`

> You've just been urgently recruited by GreHack Corp to investigate the theft of highly confidential data from their website.
> They provide you with the logs from the day of the breach, so it's up to you to find out what was exfiltrated!

- [Logs](/assets/sources/gh24/grehack_access.log)

## Solve 

The aim of this introductory Forensic challenge is to identify a secret resource that has been stolen from the GreHack website. 
A web server log file is provided in JSON format, in the following form: 

```json
{
  "level": "info",
  "ts": 1728836850.2914035,
  "logger": "http.log.access.log1",
  "msg": "handled request",
  "request": {
    "remote_ip": "224.198.129.58",
    "remote_port": "49214",
    "client_ip": "224.198.129.58",
    "proto": "HTTP/2.0",
    "method": "GET",
    "host": "grehack.fr",
    "uri": "/",
    "headers": {
      "User-Agent": [
        "Blackbox Exporter/0.25.0"
      ]
    },
    "tls": {
      "resumed": false,
      "version": 772,
      "cipher_suite": 4865,
      "proto": "h2",
      "server_name": "grehack.fr"
    }
  },
  "bytes_read": 0,
  "user_id": "",
  "duration": 0.000173248,
  "size": 8178,
  "status": 200,
  "resp_headers": {
    "Content-Type": [
      "text/html; charset=utf-8"
    ],
    "Last-Modified": [
      "Sun, 13 Oct 2024 16:20:51 GMT"
    ],
    "Accept-Ranges": [
      "bytes"
    ],
    "Content-Length": [
      "8178"
    ],
    "Server": [
      "Caddy"
    ],
    "Alt-Svc": [
      "h3=\":443\"; ma=2592000"
    ],
    "Vary": [
      "Accept-Encoding"
    ],
    "Etag": [
      "\"d4utdhjl22196b6\""
    ]
  }
}
```

If we take out the logs on the number of requests per IP to see which are the most present, here is what we observe: 

```bash
» cat grehack_access.log|  jq -r '.request.remote_ip' | uniq -c  | sort -uV
    701 46.150.36.67
     10 224.198.129.58
     11 37.102.49.170
     11 224.198.129.58
```

The IP address `46.150.36.67` made over 700 requests, making this IP a potential attacker.
Then filter on interesting responses such as HTTP 200 success codes, to see that this IP has only accessed one resource in 200.

```bash
» cat grehack_access.log|  jq -r 'select(.status == 200 and .request.remote_ip == "46.150.36.67")' | jq '.request.uri'  | uniq
"/punch_recipes"
```

By accessing this resource on the GreHack site, we obtain the flag 

<img src="/assets/images/writeups/gh24/analyst/flag.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

- Flag : `GH{Y0uR_4r3_a_4naLysT_buT_y0u_w0nT_g3T_pUnCh_r3c1p3s}`

## Resources 
- https://caddyserver.com/docs/caddyfile/directives/log

<img src="/assets/images/writeups/gh24/GH24_mascotte.svg" style="width:40%;margin-left:25%;">