---
title: "GreHack 2024 CTF - Smash Or Cache"
layout: "writeups"
date: 2024-11-16
url: "/writeups/grehack_smash_or_cache"
summary: "Writeup of the Web challenge 'Smash or Cache' at the GreHack 2024 CTF."
author: "Nishacid"
tags: 
- Writeups
- GreHack
- GreHack24
- Challenge
- Web
- Cache Poisoning
- XSS
ShowToc: true
TocOpen: true
ShowReadingTime: true
ShowBreadCrumbs: true
---

<img src="/assets/images/writeups/gh24/GH24_logocomplet.svg">

## Introduction

This year again, I was happy to be part of the organization committee for the [GreHack conference](https://grehack.fr/) and I created some challenges for the CTF. 
Organization was tricky this year, given that we had grown and sold almost 3x as many tickets as in previous years.
Thanks to all the participants, organizers and sponsors, the event was once again complety insane :fire: :green_heart:

## Challenge

- Name : `Smash or Cache`
- Category : `Web`
- Difficulty : `Easy`
- Solves : `3`
- Points : `499`
- Author : `Nishacid`

> Have you ever wondered whether a robot smashes you or passes you? With this revolutionary, speed-optimized app, it's time to find out whether or not you could have won its heart.

- [Source Code](/assets/sources/gh24/smash_or_cache.zip)

## Solve

This challenge is a simple web app that allows you to pass a name to the server, and it will return whether the name is a smash or pass.

<img src="/assets/images/writeups/gh24/smash/home.png" style="margin-left:10%;border:solid;border-color:#885FFF;">

After a quick analysis of the HTTP Headers, we can see that the application caches every request which contains a GET parameter, and caches it for 30 seconds.

<img src="/assets/images/writeups/gh24/smash/headers.png" style="margin-left:0%;border:solid;border-color:#885FFF;">


We can also see that the application sets a tracking cookie, and makes a POST request to /monitoring with the cookie and the result.
The cookie is stored and cached into the JavaScript code source.

<img src="/assets/images/writeups/gh24/smash/cookie.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

Using a new cache buster, we can inject a value into the cookie, who is reflected into the JavaScript code source and kept in the cache.

<img src="/assets/images/writeups/gh24/smash/javascript.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

Which gives us an alert.

<img src="/assets/images/writeups/gh24/smash/alert.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

According to this, we can craft a payload to exfiltrate the cookie of the bot.

<img src="/assets/images/writeups/gh24/smash/payload.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

We now need to send it to the bot with the report page, and wait for the flag.

<img src="/assets/images/writeups/gh24/smash/report.png" style="margin-left:15%;border:solid;border-color:#885FFF;">

<img src="/assets/images/writeups/gh24/smash/flag.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

After URL-decoding the flag, we can see the final flag.

- Flag : `GH{f0r_m3_iT_w1ll_b3_a_c4cH3<3}`

## Resources 
- https://portswigger.net/web-security/web-cache-poisoning
- https://book.hacktricks.xyz/pentesting-web/cache-deception#cache-poisoning
- https://www.youtube.com/watch?v=85XtXOhMd2Y (🇫🇷)

<img src="/assets/images/writeups/gh24/GH24_mascotte.svg" style="width:40%;margin-left:25%;">
