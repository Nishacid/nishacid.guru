---
title: "GreHack 2024 CTF - People Can't Avoid PCAP"
layout: "writeups"
date: 2024-11-16
url: "/writeups/grehack_people_cant_avoid_pcap"
summary: "Writeup of the network intro challenge 'People Can't Avoid PCAP' at the GreHack 2024 CTF."
author: "Nishacid"
tags: 
- Writeups
- GreHack
- GreHack24
- Challenge
- Network
- DNS
ShowToc: true
TocOpen: true
ShowReadingTime: true
ShowBreadCrumbs: true
---

<img src="/assets/images/writeups/gh24/GH24_logocomplet.svg">

## Introduction

This year again, I was happy to be part of the organization committee for the [GreHack conference](https://grehack.fr/) and I created some challenges for the CTF. 
Organization was tricky this year, given that we had grown and sold almost 3x as many tickets as in previous years.
Thanks to all the participants, organizers and sponsors, the event was once again complety insane :fire: :green_heart:

## Challenge

- Name : `People Can't Avoid PCAP`
- Category : `Network / Intro`
- Difficulty : `Very Easy`
- Solves : `38`
- Points : `50`
- Author : `Nishacid`

> Do you really think you can escape the eternal analysis of PCAP in CTF? You're a fool if you think so, but don't worry, it's not wacky I promise!

- [Network file](/assets/sources/gh24/people.pcapng)

## Solve

As the challenge suggests, we need to analyze a PCAP. The first thing we're going to do is look at the different protocols in this capture in a hierarchical way.

<img src="/assets/images/writeups/gh24/pcap/protocol_hierarchy.png" style="margin-left:2%;border:solid;border-color:#885FFF;">

A quick analysis of the HTTP protocol is inconclusive, as these are simple requests to the grehack.fr site, nothing conclusive.
Let's move on to the DNS protocol. On the last packets, we observe several TXT requests to grehack.fr and ctf.grehack.fr.

<img src="/assets/images/writeups/gh24/pcap/dns_analysis.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

If we follow the traffic, we can see that the response from ctf.grehack.fr has the flag.

<img src="/assets/images/writeups/gh24/pcap/flag_response.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

- Flag : `GH{DnS_1s_4_G00d_inTr0Tr0Tr0}`

## Resources 
- https://www.wireshark.org/docs/
- https://cabulous.medium.com/dns-message-how-to-read-query-and-response-message-cfebcb4fe817

<img src="/assets/images/writeups/gh24/GH24_mascotte.svg" style="width:40%;margin-left:25%;">