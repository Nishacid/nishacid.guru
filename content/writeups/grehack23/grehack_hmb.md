---
title: "GreHack 2023 CTF - Hold My B33r"
layout: "writeups"
date: 2023-11-18
url: "/writeups/grehack_hold_my_beer"
summary: "Writeup of the challenge 'Hold My B33r' at the GreHack 2023 CTF."
author: "Nishacid"
tags: 
- Writeups
- GreHack
- Challenge
- Steganography
- Beers
- Apéro
ShowToc: true
TocOpen: true
ShowReadingTime: true
ShowBreadCrumbs: true
---

<img src="/assets/images/writeups/gh23/gh23_logo.png">

## Introduction

This year, I was happy to be part of the organization committee for the [GreHack conference](https://grehack.fr/) and I created a few challenges for the CTF.
Thanks to all the participants and organizers, the event was once again awesome :fire: :green_heart:

For this year, we really wanted to make custom beers for the event, but we don't had a graphic designer for that and not also a brewery that was willing to make custom beers... But guess what? We found these two extraordinary people this year and managed to make some unique GreHack beers! How classy is that ? :sunglasses:

<img src="/assets/images/writeups/gh23/hmb/beer.jpg" style="margin-left:20%;border:solid;border-color:#885FFF;">

And how to make it more classy ? Of course we need to make challenges on this beer !! 

Real thanks to **Yuyu** for the design of the beers and [La Canute Lyonnaise](http://www.lacanutelyonnaise.fr) who create beers ! :beers:

## Hold My B33r 1

- Name : `Hold My B33r 1`
- Category : `Steganography`
- Solves : `23`
- Points : `50`
- Authors : `Nishacid, Yuyu`

> Have you seen it ?? We have wonderful customized beers ! These beer labels are so beautiful...

<img src="/assets/images/writeups/gh23/hmb/desc1.png" style="margin-left:20%;border:solid;border-color:#885FFF;">

These challenges are designed to be **easter eggs**, they don't need to be really difficult, and also, who really wants to do steganography ? ^^

<img src="/assets/images/writeups/gh23/hmb/etiquette.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

The first challenge is to **scan the barcode** on the left of the label, with the flag simply inside.

<img src="/assets/images/writeups/gh23/hmb/barcode.jpg" style="margin-left:0%;border:solid;border-color:#885FFF;">

- Flag : `GH{s0m3_b33r}`

## Hold My B33r 2

- Name : `Hold My B33r 2`
- Category : `Steganography`
- Solves : `11`
- Points : `150`
- Authors : `Nishacid, Yuyu`

> Have you **really** seen it ?? Are you sure of that ?? We have wonderful customized beers ! These beer are so delicious...

<img src="/assets/images/writeups/gh23/hmb/desc2.png" style="margin-left:20%;border:solid;border-color:#885FFF;">

For the second challenge, it's a bit more **steganoguessing**, but if you take a closer look at the beer's ingredients, there's something weird about it.

<img src="/assets/images/writeups/gh23/hmb/ingredients.png" style="margin-left:20%;border:solid;border-color:#885FFF;">

Take the **first character and the first digit** (case-sensitive), and **decode it in base64** to obtain the flag.

```
Ingrédients : Raisins 0°, houblon, 7 Yuzu jaunes, Malts, zestes citron, miel, Infusions zestes, Mandarine, 3 Jasmins, 9 Céréales grenobloises
```

```bash
» echo 'R0h7YjMzcmIzM3J9Cg' | base64 -d

GH{b33rb33r}
```

- Flag : `GH{b33rb33r}`