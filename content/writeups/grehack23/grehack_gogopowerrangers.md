---
title: "GreHack 2023 CTF - GoGo Power Rangers"
layout: "writeups"
date: 2023-11-18
url: "/writeups/grehack_gogo_power_rangers"
summary: "Writeup of the challenge 'GoGo Power Rangers' at the GreHack 2023 CTF."
author: "Nishacid"
tags: 
- Writeups
- GreHack
- Challenge
- Web
- Golang 
- SSTI
ShowToc: true
TocOpen: true
ShowReadingTime: true
ShowBreadCrumbs: true
---

<img src="/assets/images/writeups/gh23/gh23_logo.png">

## Introduction

This year, I was happy to be part of the organization committee for the [GreHack conference](https://grehack.fr/) and I created a few challenges for the CTF.
Thanks to all the participants and organizers, the event was once again awesome :fire: :green_heart:

## Challenge

- Name : `GoGo Power Rangers`
- Category : `Web`
- Solves : `14`
- Points : `300`
- Author : `Nishacid`
- Sources : [GoGo_Power_Rangers.zip](/assets/sources/GoGo_Power_Rangers.zip)

> A friend who's passionate about development and new technologies is training in web programming at the moment and is also a huge Power Rangers fan. So he's had some fun making a little site on the theme. He asks you to test its security before push it online.

<img src="/assets/images/writeups/gh23/ggpr/ggpr_desc.png" style="margin-left:20%;border:solid;border-color:#885FFF;">

The challenge begins with a web application, which appears to be a simple Power Ranger character selector, the app’s sources are provided.

<img src="/assets/images/writeups/gh23/ggpr/web_app.png" style="margin-left:15%;border:solid;border-color:#885FFF;">

The file tree is : 
```bash
.
├── docker-compose.yml
└── src
    ├── colors.html
    ├── Dockerfile
    ├── .env
    ├── go.mod
    ├── go.sum
    ├── main.go
    └── static
        ├── css
        │   └── main.css
        ├── img
        │   ├── blue_ranger.png
        │   ├── favicon.ico
        │   ├── green_ranger.png
        │   ├── pink_ranger.png
        │   ├── red_ranger.png
        │   └── yellow_ranger.png
        └── js
            └── jquery.min.js
```

If we examine the source code of the web page, we see that the JavaScript performs a POST request and replaces the content of `greetingMessage` with the value of the POST parameter.

```html

<div class="greeting" id="greetingMessage">
    Mystery and Stealth !
</div>

<!-- SNIPPED -->

<script>
    $(document).ready(function() {
        var bodyElement = $('body');
        var greetingElement = $('.greeting');
        var button = $('.btn');
        var colorSelectionElement = $('#colorSelection');
        var rangerImageElement = $('#rangerImage');
        var title = $('.form-header');

        function setColorTheme(color) {
            switch (color) {
                case 'red':
                    rangerImageElement.css('background-image', 'url("/static/img/red_ranger.png")');
                    rangerImageElement.css('border', '2px solid red');
                    greetingElement.css('color', 'red');
                    greetingElement.css('border', '2px solid red');
                    button.css('background-color', 'red');
                    title.css('color', 'red');
                    break;
                // SNIPPED
                default:
                    rangerImageElement.css('background-image', '');
                    greetingElement.css('color', 'black');
            }
        }

        $("#colorForm").on("submit", function(event) {
            event.preventDefault();

            var formData = $(this).serialize();
            var colorSelectionValue = colorSelectionElement.val();
            $.ajax({
                url: '/',
                type: 'POST',
                data: formData,
                success: function(data) {
                    $("#greetingMessage").html(data);
                    setColorTheme(colorSelectionValue);
                }
            });
        });
    });
</script>
```

According to the JavaScript sources, when we select a Power Ranger, this POST request is sent to the back-end :

<img src="/assets/images/writeups/gh23/ggpr/intercept.png" style="margin-left:10%;border:solid;border-color:#885FFF;">

This POST request is used to update the color of the **JSON Web Token (JWT)**, but we can modify the color value.

<img src="/assets/images/writeups/gh23/ggpr/jwt_update.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

This means we can control the content of the JWT without having the signature, but we can't create new fields because it's escaped. We can use this new JWT with a simple GET request on the home page.

<img src="/assets/images/writeups/gh23/ggpr/jwt_reflected.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

So we're controlling an input that's reflected on the web page. When we look at the back-end source code, it seems that a **template** is used to print this input.

```golang
[...]
if r.Method == "POST" {
    color := r.FormValue("color")
    claims := &Claims{
        Color: color,
    }

    [...]

    // Add the personalized message
    var sentence string
    switch color {
    case "red":
        sentence = "Strength and Power!"
    // SNIPPED
    default:
        sentence = "Welcome, " + color + " Ranger!"
    }

    colorTemplate := claims.Color
    
    [...]

    parsedColorTemplate := tpl.String()

    data := struct {
        Greeting            string
        dumObj              *dummyObj
        ParsedColorTemplate string
    }{
        Greeting:            "Welcome, Ranger!",
        dumObj:              dumObj,
        ParsedColorTemplate: parsedColorTemplate,
    }

    t, _ := template.ParseFiles("colors.html")
    t.ExecuteTemplate(w, "colors.html", data)
```

We can think of a **Server Side Template Injection (SSTI)** vulnerability, which can be confirmed with these payloads:

<img src="/assets/images/writeups/gh23/ggpr/payload_data.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

<img src="/assets/images/writeups/gh23/ggpr/payload_printf.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

According to the sources, the flag is stored in the `.env` file, and we now need to exploit this vulnerability to leak this flag.

```bash
JWT_SECRET=weaksecret
FLAG=GH{fakeflag}
PORT=5000
```

And as we can see in the golang code, there's a method called in the template function to retrieve the `JWT_SECRET` variable from the environment.

```golang
func (obj *dummyObj) Readenv(key string) string {
	return os.Getenv(key)
}

// snipped

func ColorSelection(w http.ResponseWriter, r *http.Request) {
    // snipped
		tkn, err := jwt.ParseWithClaims(tknStr, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("JWT_SECRET")), nil
		})
```

Finally, we can use the **SSTI** vulnerability to retrieve the environment flag.

<img src="/assets/images/writeups/gh23/ggpr/flag.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

- Flag : `GH{Goo_Gooo_P0w3r_R4ng3333rs!!}`

## Ressources 
- https://blog.takemyhand.xyz/2020/06/ssti-breaking-gos-template-engine-to 
- https://book.hacktricks.xyz/pentesting-web/ssti-server-side-template-injection#ssti-in-go 
- https://dev.to/pirateducky/ssti-method-confusion-in-go-517p 
- https://www.onsecurity.io/blog/go-ssti-method-research/