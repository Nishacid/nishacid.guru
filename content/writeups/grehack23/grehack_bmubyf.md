---
title: "GreHack 2023 CTF - Beer Me Up Before You Format"
layout: "writeups"
date: 2023-11-18
url: "/writeups/grehack_beer_me_up_before_you_format"
summary: "Writeup of the challenge 'Beer Me Up Before You Format' at the GreHack 2023 CTF."
author: "Nishacid"
tags: 
- Writeups
- GreHack
- Challenge
- Web
- Format String 
- Python
- File Read
- IDOR
ShowToc: true
TocOpen: true
ShowReadingTime: true
ShowBreadCrumbs: true
---

<img src="/assets/images/writeups/gh23/gh23_logo.png">

## Introduction

This year, I was happy to be part of the organization committee for the [GreHack conference](https://grehack.fr/) and I created a few challenges for the CTF.
Thanks to all the participants and organizers, the event was once again awesome :fire: :green_heart:
Real thanks to [Elweth](https://twitter.com/Elweth_) who make a big part of this challenge.

## Challenge

- Name : `Beer Me Up Before You Format`
- Category : `Web`
- Solves : `12`
- Points : `350`
- Author : `Elweth, Nishacid`
- Sources : [Beer_Me_Up_Before_You_Format.zip](/assets/sources/Beer_Me_Up_Before_You_Format.zip)

> *Beer me up before you go-go, Leave a pint for me, don’t be a no-no. Beer me up before you go-go, I don't want to miss out when you pour that froth*

<img src="/assets/images/writeups/gh23/bmubyf/desc.png" style="margin-left:20%;border:solid;border-color:#885FFF;">

With this challenge, the `Beer_Me_Up_Before_You_Format.zip` archive is provided. This is the source code for the active application on https://beer-me-up-before-you-format.ctf.grehack.fr/. \
This archive contains the following tree structure :

```bash
.
├── app
│   ├── api
│   │   ├── queries.py
│   │   └── Secret.py
│   ├── app.py
│   ├── db
│   │   └── database.db
│   └── templates
│       ├── index.html
│       └── secret.html
├── docker-compose.yml
├── Dockerfile
├── flag.txt
└── requirements.txt
```

This is a python application, using the **Flask Framework** to start a web server. \
An analysis of the source code reveals the following endpoints:

```python
@app.route("/")

@app.route("/api/<endpoint>/<id_user>")

@app.route("/api/password-reset", methods=["POST"])

@app.route("/api/login", methods=["POST"])

@app.route("/api/admin", methods=["POST"])

@app.route(f"/api/{SECRET_ENDPOINT}", methods=["POST"])
```

The `api/queries.py` file contains the SQL queries made to the SQLite database, which appears to be secured using prepared queries. 

In the `api/secret.py` file, we note the presence of the "**Secret**" class, as well as a `SECRET_ENDPOINT` global variable containing the route we're going to find.

```python
import os

SECRET_ENDPOINT = "secret"

class Secret:
	def __init__(self, secret):
		self.secret = secret

	def __repr__(self):
		return f"The secret endpoint is : /{self.secret} !"
```

### 403 Bypass

The first accessible endpoint is :

```python
# To manage multi-endpoints
@app.route("/api/<endpoint>/<id_user>")
def parse(endpoint, id_user):
    if endpoint.lower() in ENDPOINTS:
        if endpoint == "users":
            return jsonify(error="This endpoint is for admins only."), 403
        return jsonify(get_user(int(id_user)))
    else:
        return jsonify(error="This page does not exists."), 404
```

The function performs parsing on the `endpoint` value in order to display the user information linked to the `user_id` also passed as a parameter.
However, we are not allowed to query the URI `/api/users/<id>`, since it checks whether the endpoint is set to  "users", which seems to be reserved for admins, and returns a 403 error.

<img src="/assets/images/writeups/gh23/bmubyf/api_users.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

However, the application does not take into account the "case sensitive" part of endpoints, and it is possible to bypass the following condition by using uppercase letters.

```python
if endpoint == "users":
    return jsonify(error="This endpoint is for admins only."), 403
return jsonify(get_user(int(id_user)))
```

It gives : `/api/uSers/1` : 

<img src="/assets/images/writeups/gh23/bmubyf/api_bypass.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

### Insecure Direct Object Reference

Now that we have access to the endpoint users, we can list all the application's users.
The aim is to target admin accounts in particular, characterized by the `ADMIN` role in the JSON response.

To do this, we can use Burp's intruder to increment the `user_id` part, while at the same time match `ADMIN` in the server response.

<img src="/assets/images/writeups/gh23/bmubyf/grep_admin.png" style="margin-left:15%;border:solid;border-color:#885FFF;">

Once the intruder is complete, we recover the list of users with the `ADMIN` role.

<img src="/assets/images/writeups/gh23/bmubyf/intruder_admin.png" style="margin-left:5%;border:solid;border-color:#885FFF;">

Another interesting result in the api response is the `token` part, which appears to be a **UUID**.

### Password Reset abuse

This returned token is far from uninteresting, as it is used to reset the password.

The endpoint `/api/password-reset` expects 2 parameters: `token` and `password`. 
Visit the `update_password` function, we'll notice that it's possible to update an account's password, assuming we know the token. 

```python
@app.route("/api/password-reset", methods=["POST"])
def password_reset():
    json = request.get_json()
    try:
        token = json["token"]
        password = json["password"]
        if update_password(token, password):
            return jsonify(success="The password has been reset.")
        else:
            return jsonify(error="An error has occured.")
    except Exception as e:
        print(e)
        return jsonify(error="Parameter 'token' or 'password' are missing.")
```

In this way, we can forge the following query to reset an administrator's password :

<img src="/assets/images/writeups/gh23/bmubyf/password_reset.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

Once the password has been updated, it is possible to connect to the compromised account and thus obtain a valid session token:

<img src="/assets/images/writeups/gh23/bmubyf/jwt.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

### Python Format String

To access the admin endpoint, you need to authenticate yourself by providing the token previously retrieved from the `X-Api-Key` header.
First, the token's integrity is checked, and the "**ROLE**" part of the data must be set to "**ADMIN**".
Next, the API retrieves the `secret` variable contained in the JSON sent.

<img src="/assets/images/writeups/gh23/bmubyf/secret.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

If we take a closer look at how the passed content is rendered in the template, we notice that a **format string** is used:

```python
return f"The secret endpoint is : /{self.secret} !"
```

Using this syntax in the format string is a vulnerability, as it allows access to the object's properties directly in the **format string**.

- https://realpython.com/python-string-formatting/
- https://lucumr.pocoo.org/2016/12/29/careful-with-str-format

We can confirm the vulnerability by sending `{secret}` to call the object itself.

<img src="/assets/images/writeups/gh23/bmubyf/format_object.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

Here we can see that the `repr` method (equivalent to a `toString`) is called 2 times, since we have 2 times the output.

Let's dig a little deeper using a python debugger.
To do this, we're going to use the [ipdb](https://pypi.org/project/ipdb/) and [rich](https://rich.readthedocs.io/en/stable/introduction.html) libraries. 
With these libraries, we'll set a **breakpoint** in the code, so we can inspect the `secret` variable and see what we can learn from it.
Let's add the following lines to the `app.py` code:

```python
import ipdb, rich

secret = request.get_json()["secret"]
secret = Secret(secret)
ipdb.set_trace()
```

When you restart the code, the process pauses and offers us a prompt.

<img src="/assets/images/writeups/gh23/bmubyf/ipdb.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

We have access to a python-like console, allowing us to perform actions on the program. For example, we can display the contents of the `secret` variable :

<img src="/assets/images/writeups/gh23/bmubyf/print_secret.png" style="margin-left:20%;border:solid;border-color:#885FFF;">

Using the **rich** library, you can list all the methods to which the object `secret` has access

<img src="/assets/images/writeups/gh23/bmubyf/rich_inspect.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

It is therefore possible to call all these methods, including the `init` method for building the object `Secret`.

<img src="/assets/images/writeups/gh23/bmubyf/secret_init.png" style="margin-left:10%;border:solid;border-color:#885FFF;">

Remember that our precious flag is stored in the global variables of the `Secret` object,

<img src="/assets/images/writeups/gh23/bmubyf/secret_endpoint.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

And with Burp Suite : 

<img src="/assets/images/writeups/gh23/bmubyf/secret_burp.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

### File Read

Now we have retrive the `secret` endpoint, we can access with the new feature used to read file from system.

```python
filename = urllib.parse.unquote(request.get_json()['filename'])
data = "This file doesn't exist"
bad_chars = ["../", "\\", "."]
is_safe = all(char not in filename for char in bad_chars)

if is_safe:
    filename = urllib.parse.unquote(filename)
    if os.path.isfile('./'+ filename):
        with open(filename) as f:
            data = f.read()
return jsonify(data)
```

In the `Dockerfile` file, we can see the flag is located at `/flag.txt`.

```dockerfile
COPY flag.txt /flag.txt
```

But with the filter, we can't read it directly :

<img src="/assets/images/writeups/gh23/bmubyf/filename.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

The filter uses the `urllib.parse.unquote()` two times, so we can **double url-encoded** it (don't forget the dot), bypass this filter and leak the flag : 

<img src="/assets/images/writeups/gh23/bmubyf/file_read.png" style="margin-left:0%;border:solid;border-color:#885FFF;">

- Flag : `GH{F0rm4t_Str1ng_t0_D4T4_L34K}`


## Ressources
- https://flask.palletsprojects.com/en/2.3.x/
- https://cheatsheetseries.owasp.org/cheatsheets/Insecure_Direct_Object_Reference_Preven
tion_Cheat_Sheet.html
- https://podalirius.net/en/articles/python-format-string-vulnerabilities/
- https://ctftime.org/writeup/10851
- https://www.root-me.org/fr/Challenges/App-Script/Python-format-string