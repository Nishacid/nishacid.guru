---
title: "Burp Suite Certified Practitioner"
layout: "articles"
date: 2023-02-25
url: "articles/bscp"
summary: "Burp Suite Certified Practitioner Certification Review"
author: "Nishacid"
tags: 
- BurpSuite Certified Practitioner
- Certification
- Retex 
- Article
- Review
ShowToc: true
TocOpen: true
ShowReadingTime: true
ShowBreadCrumbs: true
---

Hello, 

I recently completed and successfully passed the **[Burp Suite Certified Practitioner](https://portswigger.net/web-security/certification)** exam, and several people have asked me how the certification works, if the exam is interesting, tips on how to properly prepare.

I will try in this review to answer as many questions as possible, and I hope to motivate as many people as possible to try to pass the certification!

## The exam

Let's talk a little bit about the test itself. So there are two web applications, each composed of 3 vulnerabilities.

1. Access to a user account
2. Elevate your privileges to administrator
3. Read the contents of a file on the server

It is normally impossible to access a step without having passed the previous one. The goal is to pass all the steps in 4 hours, you will not get the certification if you miss one of these steps.

It is required to have a valid [Burp Suite Professional](https://portswigger.net/burp/pro) license during the exam, otherwise you will be refused from the exam. You don't have a license and you think it's expensive just to take the exam? You are right and that's why it is even possible to take the exam with [BurpSuite Professional trial version](https://portswigger.net/burp/pro/trial). Just enter your email address and receive the trial version, so no more excuses !

One point I found really unfortunate is that Examity, the monitoring platform used by PortSwigger, requires the usage of Windows and Google Chrome, as I'm on Ubuntu with Firefox it was fine for me ! I made a Windows VM for the occasion, it's a shame not to support Firefox ^^

During the exam, you will be confronted with a lot of false positives, during my first attempt this made me lose a lot of time. You will have to learn to quickly identify an exploitable vulnerability from a false one. For example, it can happen that you can have an XSS vulnerability without it being delivered to the victim, which can be quite frustrating at times 😄

Don't waste too much time if you get stuck on a vulnerability that seems exploitable, look for another one instead!

## Requirements

I obviously recommend to everyone who would like to take this certification to complete all the **Apprentice** and **Practitioner** labs, even if you already know how to exploit these vulnerabilities, it is important to understand the methodology used that we will be able to recover during the exam.

Recently, PortSwigger introduced a new type of tracking in our user dashboard called "Exam preparation steps" to help with [preparing for the exam](https://portswigger.net/web-security/certification/how-to-prepare), which I thought was really well made and would recommend using to maximize success.

![burp_challenge](/assets/images/articles/bscp/burp_challenge.png)

So we have one lab from each different category (XSS, SQLi, File Upload...), **8 labs called "specific "** to the exam which are quite similar to the ones present on the day of the evaluation. Then, we have to do **5 mystery labs**, it's just a random lab from the **~200 proposed**, without knowing which one has been selected. I would advise you to do a lot more anyway, 5 is too few. Then we have **1 practice exam**, which is just a dummy exam. I would advise you to do the practice exam at least once, even if you don't necessarily finish it, but in sure to have the methodology to adopt.

## My attempts 

First of all, I personally passed the exam after 3 attempts. At first, I took the exam when the certification was just launched. At that time, the total duration for completing the labs was 3 hours only for 2 labs (6 challenges), it has since been increased to a duration of 4 hours which is not to our displeasure and makes things more accessible.

On my first attempt, I passed 3/3 challenges on the first application and 0/3 on the second, I did not pass the first step.

For the second attempt PortSwigger added an hour more, but the result was the same, the first one was completely pwned quite easily, and the second one impossible to pass the first step...

![attempt](/assets/images/articles/bscp/attempt.png)

To be honest after that I was a bit frustrated and didn't want to take the certification again... I waited a few long months before getting back to it, but after a while I got motivated to do the last labs I had left, and then there was the `#BurpChallenge` which allowed me to win a voucher to take the certification. Once I got the voucher, I took the exam again, and this time, I passed it pretty easily, I just got really stuck on one step, but otherwise it was fine.

![exam_end](/assets/images/articles/bscp/exam_end.png)

After about 3 days of waiting, I finally received my validation and certificate of completion!

![certification](/assets/images/articles/bscp/certification.png)

## A little tool, WSAAR

I started to created a tool for personal use, at the beginning of the certification when it was still only 3 hours for 6 challenges, because I was very short of time and I needed a tool that would make me recognize the labs to go faster. In no way my goal was to make a tool that was going to make me do labs automatically, that's why there is no payload in this script, it's only to save time on the reconnaissance related to labs.

I know, the code of this script is not perfect, as I said before I had made this script quickly as my own, I publish this code hoping to give a starting point to someone to make maybe something cleaner or to have a little tool to help with the reconnaissance. If you want to improve it, don't hesitate to contribute

- [WSAAR](https://github.com/Nishacid/WSAAR)

![wsaar](/assets/images/articles/bscp/wsaar.png)

## Some tips

- Don't hesitate to learn how to use the BurpSuite scanner, the applications are basically designed for that.
- You can use other tools than BurpSuite, don't forget it!
- Don't forget to add extensions that will help you on your BurpSuite as :
	- [Collaborator Everywhere](https://portswigger.net/bappstore/2495f6fb364d48c3b6c984e226c02968)
	- [Hackvertor](https://portswigger.net/bappstore/65033cbd2c344fbabe57ac060b5dd100)
	- [HTTP Request Smuggler](https://portswigger.net/bappstore/aaaa60ef945341e8a450217a54a11646)
	- [Java Deserialization Scanner](https://portswigger.net/bappstore/228336544ebe4e68824b5146dbbd93ae)
	- [Web Cache Deception Scanner](https://portswigger.net/bappstore/7c1ca94a61474d9e897d307c858d52f0)
	- [Param Miner](https://portswigger.net/bappstore/17d2949a985c4b7ca092728dba871943)
	- …
- It is important to be able to quickly identify a false positive from a real vulnerability, no time should be wasted during this exam.
- Take your time before launching the exam, do all the Apprentice and Practitioner labs, understand the methodology of the labs.
- Do a lot of mystery labs, the exam plays on randomness, be prepared for that.
- Take the practice exam at least once, to see what to be prepared for.
- Take notes before the exam on the vulnerabilities discussed (how to detect them, exploit them) and during the exam (don't do a report, you don't have time).
- Prepare your environment **BEFORE** starting the exam (notes, wordlists, tools...).
- You have a methodology to follow, don't try to find a **SSRF** or **Command Injection** when you don't even have the user.

## Conclusion

I can only recommend this certification, because of its very accessible price, its totally free resources or even all the technicality and web diversity it covers.

The exam itself is not that difficult, as long as you have passed the recommended labs (it's free, you can do as many as you want).

I would have appreciated seeing a small report part (with additional time), in order to get the participants accustomed to taking notes during intrusion tests or other. I also liked the fact that you can start the exam absolutely whenever you want, at any time, it's very useful to have no time restrictions!

I hope to have at least clarified the subject for you, and maybe motivated you to take the certification! If you have any question, don't hesitate to send me a DM on [Twitter](https://twitter.com/Nishacid) or Discord (`Nishacid#1337`) it will be a pleasure to talk about it with you !

*PS: big thanks to [Mika](https://bwlryq.net/) for the review (and all the typos ^^) check out his blog too !*